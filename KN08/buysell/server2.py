from flask import Flask, request, jsonify
from flask_cors import CORS
import requests

app = Flask(__name__)
CORS(app)

# BASE_URL = 'http://127.0.0.1:8090/Account'
BASE_URL = 'http://account:8080/Account'

def check_balance(id,amount):
    url = BASE_URL + '/Cryptos'
    params = {'userId': id}
    headers = {'accept': 'application/json'}
    response = requests.get(url, headers=headers, params=params)
    data=response.json()
    print(data)
    if (int(data)>=amount):
        return True
    return False

def removeFromUser(id,amount):
    url = BASE_URL + '/RemoveCrypto'
    params = {'userId': id, 'amount':amount}
    headers = {'accept': 'application/json'}
    response = requests.post(url, headers=headers, params=params)
    data=response.json()
    print("Remove :",data)

def addToUser(id,amount):
    url = BASE_URL + '/AddCrypto'
    params = {'userId': id, 'amount':amount}
    headers = {'accept': 'application/json'}
    response = requests.post(url, headers=headers, params=params)
    data=response.json()
    print("Add :",data)

@app.route('/buy', methods=['GET', 'POST'])
def buy():
    if request.method == 'POST':
        data = request.json
        print(data)

        id= data['id']
        amount = int(data['amount'])

        addToUser(id,amount)
        
        return jsonify({"message": "Bought", "amount": amount}), 200
    return jsonify({"message": "Send endpoint"}), 200

@app.route('/sell', methods=['GET', 'POST'])
def sell():
    if request.method == 'POST':
        data = request.json
        print(data)

        id= data['id']
        amount = int(data['amount'])

        if(check_balance(id,amount)):
            removeFromUser(id,amount)
        else:
            return jsonify({"message": "Balance to Low"}), 401
        
        return jsonify({"message": "Sold", "amount": amount}), 200
    return jsonify({"message": "Send endpoint"}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8002)
