# KN01

![](assets/20240613_143337_image.png)

![](assets/20240613_143412_image.png)

## B)

```
docker search ubuntu
docker search nginx
```

`docker run -d -p 80:80 docker/getting-started`

- `-d` Detached mode, console does not open
- `-p 80:80` maps the internal port 80 to the external port 80
- docker/getting-started name of the image

### Nginx

```
docker pull nginx
docker create -p 8081:80 nginx:latest
docker start 1535a3923cbb81f28e5fc9dd55fce079b969451646ca7871c264e487054a14e8
```

![alt text](assets/image.png)

### Ubuntu

It was downloaded but didn't start.

`-it` starts the interactive tty

Now we have a terminal on the ubuntu container.

### Nginx 2

`docker exec -it 1535a3923cbb81f28e5fc9dd55fce079b969451646ca7871c264e487054a14e8 /bin/bash`

![alt text](assets/image-1.png)

![](assets/20240613_154148_image.png)

`docker stop 1535a3923cbb81f28e5fc9dd55fce079b969451646ca7871c264e487054a14e8`

## C)

![](assets/20240613_154739_image.png)

`docker tag nginx:latest killua404/m347:nginx`

We add a tag to the nginx image.

`docker push killua404/m347:nginx`

The image with the following tag gets pushed.

![](assets/20240613_155715_image.png)

